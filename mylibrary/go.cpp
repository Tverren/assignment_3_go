#include "go.h"
#include "randomai.h"


namespace mylib {
namespace go {
namespace priv {

Board_base::Board_base( Size size ) {
    resetBoard(size);
}

Board_base::Board_base(Board::BoardData&& data,
                       StoneColor turn,
                       bool was_previous_pass)
    : _current{std::forward<Board::BoardData>(data),
               turn,
               was_previous_pass}
{
    // ... init ...
}
Board_base::Position::Position(Board::BoardData&& data,
                               StoneColor trn,
                               bool prev_pass)
    : board{data},
      turn{trn},
      was_previous_pass{prev_pass} {}

void Board_base::resetBoard(Size size) {
    _current.board.clear();
    _size = size;
    _current.turn = StoneColor::White;
}

Size Board_base::size() const {
    return _size;
}

bool Board_base::wasPreviousPass() const {
    return _current.was_previous_pass;
}

StoneColor Board_base::turn() const {
    return _current.turn == StoneColor::Black ? StoneColor::White : StoneColor::Black;
}

//Check if choosen position has a north, if it is inside the given board size
//and if the north spot is occupied
bool Stone_Base::hasNorth() const
{
    //check the board size
    auto board_size = _board->size();
    //point N north of current point
    //                   x + 0          y - 1
    auto N = Point{ _point.first, _point.second-1};

    //check if point is inside board, if yes return true else return false.
    if (!(
                (N.first <= board_size.first && N.first >= 1)
                &&
                (N.second <= board_size.second && N.second >= 1)
                ))
        return false;

    //check if point have stone, if yes return true else return false
    if(!_board->hasStone(N))
        return false;

    return true;
}

//Check if choosen position has a east, if it is inside the given board size
//and if the east spot is occupied
bool Stone_Base::hasEast() const
{
    //check the board size
    auto board_size = _board->size();
    //point E east of current point
    //                   x - 1          y - 0
    auto E = Point{ _point.first + 1, _point.second};

    //check if point is inside board, if yes return true else return false.
    if (!(
                (E.first <= board_size.first && E.first >= 1)
                &&
                (E.second <= board_size.second && E.second >= 1)
                ))
        return false;

    //check if point have stone, if yes return true else return false
    if(!_board->hasStone(E))
        return false;
    return true;
}

//Check if choosen position has a west, if it is inside the given board size
//and if the north spot is occupied
bool Stone_Base::hasWest() const
{
    //check the board size
    auto board_size = _board->size();
    //point W west of current point
    //                   x + 1          y - 0
    auto W = Point{ _point.first - 1, _point.second};

    //check if point is inside board, if yes return true else return false.
    if (!(
                (W.first <= board_size.first && W.first >= 1)
                &&
                (W.second <= board_size.second && W.second >= 1)
                ))
        return false;

    //check if point have stone, if yes return true else return false
    if(!_board->hasStone(W))
        return false;
    return true;
}

//Check if choosen position has a south,
//if it is inside the given board size
//and if the south spot is occupied
bool Stone_Base::hasSouth() const
{
    //check the board size
    auto board_size = _board->size();
    //point S south of current point
    //                   x + 0          y - 1
    auto S = Point{ _point.first, _point.second+1};

    //check if point is inside board, if yes return true else return false.
    if (!(
                (S.first <= board_size.first && S.first >= 1)
                &&
                (S.second <= board_size.second && S.second >= 1)
                ))
        return false;

    //check if point have stone, if yes return true else return false
    if(!_board->hasStone(S))
        return false;
    return true;
}

StoneColor Stone_Base::color()
{
    return _color;
}

Stone_Base::Stone_Base(StoneColor sColor,
                       Point pPosition,
                       std::shared_ptr<Board> sharedBoard)
{
    _color = sColor;
    _point = pPosition;
    _board = sharedBoard;
}

} // END namespace priv

void Board::placeStone(std::shared_ptr<Stone> stone) {
    _current.board[stone->Spot()] = stone;
    _current.turn = turn();
}

void Board::passTurn() {
    _current.turn = turn();
}

//spot has a stone
bool Board::hasStone(Point intersection) const {
    return _current.board.count(intersection);
}

std::shared_ptr<Stone> Board::stone(Point intersection) const {
    return _current.board.at(intersection);
}

//checks for stones. and can not place stone on stone.
bool Board::isNextPositionValid(Point intersection) const {
    //checks if there are stones around the intersection
    if(isSuicide(intersection))
        return false;

    //can not place a stone over a stone(using mouse-click)
    return !Board::hasStone(intersection);
}

// This function checks whether or not a position is free,
// AND whether or not it is on the board
bool Board::isFree(Point point) const
{
    // False if outside horizontal direction
    if (point.first < 0 || point.first >= size().first)
        return false;
    // False if outside, vertical direction
    if (point.second < 0 || point.second >= size().second)
        return false;
    // Cannot be free if there is a stone
    if(hasStone(point))
        return false;

    return true;
    /*
          return !hasStone(_point)&&_point.first <= size().first && _point.first > 0
                &&_point.second <= size().second && _point.second > 0;
          */
}

bool Board::currentStone(Point intersection) const
{
    /*      //helpnotes:
          if(spot_where_current_player_place_stone isFree())
                if(hasNorth_is_inside_the_board)
                   if(hasNorth_hasStone == to current_player_stone)
                     return true;//if the color of north stone equals current stone
                   return false;//if the point is not inside the board

                if(hasEast_is_inside_the_board)
                    if(hasEast_hasStone == to current_player_stone)
                     return true;//if the color of east stone equals current stone
                   return false;//if the point is not inside the board

                if(hasWest_is_inside_the_board)
                    if(hasWest_hasStone == to current_player_stone)
                     return true;//if the color of west stone equals current stone
                   return false;//if the point is not inside the board

                if(hasSouth_is_inside_the_board)
                    if(hasSouth_hasStone == to current_player_stone)
                     return true;//if the color of south stone equals current stone
                   return false;//if the point is not inside the board
          return true;
         //the code:*/

    //Current stone postion
    auto P = Point(intersection.first,intersection.second);
    //points to the north/east/west/south of current stone
    auto N = Point(intersection.first,intersection.second-1);/*North*/
    auto E = Point(intersection.first+1,intersection.second);/*East*/
    auto W = Point(intersection.first-1,intersection.second);/*West*/
    auto S = Point(intersection.first,intersection.second+1);/*South*/

    //checks if intersection is occupied
    if(!isFree(Point(P))) return false;

    //if intersection not occupied go in here.

    /*straks return er enten true eller false så termineres metoden og fortsetter ikke
    * FIX THIS
    * !!!!
   */
    if(hasStone(Point(N)))
    {
        if(stone(N)->color() == stone(P)->color())
        {
            return true;//if they are of the same color
        }
        return false;//if there are not stones or the stones are not of the same color
    }

    if(hasStone(Point(E)))
    {
        if(stone(E)->color() == stone(P)->color())
        {
            return true;//if they are of the same color
        }
        return false;//if there are not stones or the stones are not of the same color
    }

    if(hasStone(Point(W)))
    {
        if(stone(W)->color() == stone(P)->color())
        {
            return true;//if they are of the same color
        }
        return false;//if there are not stones or the stones are not of the same color
    }

    if(hasStone(Point(S)))
    {
        if(stone(S)->color() == stone(P)->color())
        {
            return true; //if they are of the same color
        }
        return false;//if there are not stones or the stones are not of the same color
    }
    return true;; //if intersection is outside the board or free
}

//checks if there are stones around the intersection
bool Board::isSuicide(Point intersection) const
{
    auto N = Point(intersection.first,intersection.second-1);/*North*/
    auto E = Point(intersection.first+1,intersection.second);/*East*/
    auto W = Point(intersection.first-1,intersection.second);/*West*/
    auto S = Point(intersection.first,intersection.second+1);/*South*/
    //checks if intersection is surrounded
    if(
            isFree(Point(N))
            ||isFree(Point(E))
            ||isFree(Point(W))
            ||isFree(Point(S))
            )
        return false;/*if intersection IS NOT surrounded*/

    return true; /*if intersection IS surrounded*/
}

Point Stone::Spot()
{
    return _point;
}

void Boundary::createBoundary()
{

}

void Boundary::addBoundary()
{

}

void Boundary::removeBoundaryPoint()
{

}

void Boundary::removeBoundary()
{

}

Block::Block(std::set<Point> storage, std::set<Boundary> boundaries)
{
    _storage = storage;
    _boundaries = boundaries;
}

void Block::createBlock()
{

}

void Block::addBlock()
{

}

void Block::removeBlock()
{

}

Engine::Engine()
    : _board{std::make_shared<Board>()}, _game_mode{}, _active_game{false},
      _white_player{nullptr}, _black_player{nullptr} {}

//HUMAN VS HUMAN
void Engine::newGame(Size size) {
    _board->resetBoard(size);
    _game_mode = GameMode::VsPlayer;
    _active_game = true;
    _white_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::Black);
    _black_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::White);
}

//HUMAN VS AI
void Engine::newGameVsAi(Size size) {
    _board->resetBoard(size);
    _game_mode = GameMode::VsAi;
    _active_game = true;
    _white_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::Black);
    _black_player = std::make_shared<RandomAi>(shared_from_this(),   StoneColor::White);
}

//AI VS AI
void Engine::newGameAiVsAi(Size size) {
    _board->resetBoard(size);
    _game_mode = GameMode::Ai;
    _active_game = true;
    _white_player = std::make_shared<RandomAi>(shared_from_this(),StoneColor::Black);
    _black_player = std::make_shared<RandomAi>(shared_from_this(),StoneColor::White);
}

void Engine::newGameFromState(Board::BoardData&& board,
                              StoneColor turn,
                              bool was_previous_pass) {
    _board = std::make_shared<Board>(std::forward<Board::BoardData>(board),turn,was_previous_pass);
    _game_mode = GameMode::VsPlayer;
    _active_game = true;
    _white_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::Black);
    _black_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::White);
}

const std::shared_ptr<Board> Engine::board() const {
    return _board;
}

const GameMode& Engine::gameMode() const {
    return _game_mode;
}

StoneColor Engine::turn() const {
    return _board->turn();
}

const std::shared_ptr<Player> Engine::currentPlayer() const {
    if(      turn() == StoneColor::Black ) return _black_player;
    else if( turn() == StoneColor::White ) return _white_player;
    else                              return nullptr;
}

bool Engine::placeStone(Point intersection) {
    auto success = validateStone(intersection);
    if(success)
    {
        auto stone = std::make_shared<Stone> (_board->turn(), intersection, _board);
        _board->placeStone(stone);
    }

    return success;
}

void Engine::passTurn() {
    if(board()->wasPreviousPass()) {
        _active_game = false;
        return;
    }
    _board->passTurn();
}

void Engine::nextTurn(std::chrono::duration<int,std::milli> think_time) {
    if( currentPlayer()->type() != PlayerType::Ai) return;
    auto p = std::static_pointer_cast<AiPlayer>(currentPlayer());
    p->think( think_time );
    if( p->nextMove() == AiPlayer::Move::PlaceStone )
        placeStone( p->nextStone() );
    else
        passTurn();
}

bool Engine::isGameActive() const {
    return _active_game;
}

bool Engine::validateStone(Point pos) const {
    return _board->isNextPositionValid(pos);
}
} // END namespace go
} // END namespace mylib
