#ifndef GO_H
#define GO_H

//stl
#include <map>
#include <memory>
#include <utility>
#include <chrono>
#include  <set>

namespace mylib {
namespace go {

class Stone;
class Board;
class Boundary;
class Block;
struct GameState;
class Player;
class AiPlayer;
class HumanPlayer;
class Engine;

using time_type = std::chrono::duration<int,std::milli>;

constexpr time_type operator"" _ms (unsigned long long int milli) { return time_type(milli); }
constexpr time_type operator"" _s  (unsigned long long int sec)   { return time_type(sec*1000); }

enum class StoneColor {
    Black       = 0,
    White       = 1
};

enum class PlayerType {
    Human       = 0,
    Ai          = 1
};

enum class GameMode {
    VsPlayer    = 0,
    VsAi        = 1,
    Ai          = 2
};

using Point       = std::pair<int,int>;
using Size        = std::pair<int,int>;
using BoardData   = std::map<Point,StoneColor>;

// INVARIANTS:
//   Board fullfills; what does one need to know in order to consider a given position.
//   * all stones and their position on the board
//   * who places the next stone
//   * was last move a pass move
//   * meta: blocks and freedoms

// START namespace priv
namespace priv {
//START class board_base
class Board_base {
public:
    using BoardData = std::map<Point,std::shared_ptr<Stone>>;

    struct Position {
        BoardData          board;
        StoneColor         turn;
        bool               was_previous_pass;
        Position () = default;
        explicit Position ( BoardData&& data,
                            StoneColor turn,
                            bool was_previous_pass);
    }; // END class Position

    // Constructors
    Board_base() = default;
    explicit Board_base( Size size );
    explicit Board_base( BoardData&& data,
                         StoneColor turn,
                         bool was_previous_pass );
    // Board data
    Size                          size() const;
    bool                          wasPreviousPass() const;
    StoneColor                    turn() const;
    // Board
    void                          resetBoard( Size size );
    // Board
    Size                          _size;
    Position                      _current;

}; // END base class Board_base

//START class stonebase
class Stone_Base{
public:
    StoneColor                    _color;
    Point                         _point;
    std::shared_ptr<Board>        _board;

    //constructor
    Stone_Base(StoneColor sColor, go::Point pPosition, std::shared_ptr<Board> sharedBoard);
    //Actions
    bool                          hasNorth() const;
    Point                         North() const;
    bool                          hasEast() const;
    Point                         East() const;
    bool                          hasWest() const;
    Point                         West() const;
    bool                          hasSouth() const;
    Point                         South() const;

    StoneColor color();

};// END class stonebase
} // END namespace priv

//START class stone
class Stone : private priv::Stone_Base{
public:

    Point Spot();

    //using the constructor
    using Stone_Base::Stone_Base;

    // Make visible from Stone_Base
    using Stone_Base::North;
    using Stone_Base::hasNorth;
    using Stone_Base::East;
    using Stone_Base::hasEast;
    using Stone_Base::West;
    using Stone_Base::hasWest;
    using Stone_Base::South;
    using Stone_Base::hasSouth;

    using Stone_Base::color;

};// END class stone

//START class block
class Block{
public:
    std::set<Point>                   _storage;
    std::set<Boundary>                _boundaries;
    std::set<Stone>                   _stone;

    std::shared_ptr<Board>            _board;

    //constructor
    Block(std::set<Point> storage, std::set<Boundary> boundaries);

    // Actions:
    void createBlock(); //creates a block when a stone is added to board
    void addBlock(); //adds one (or more) blocks/stones togheter
    void removeBlock(); //removes the block when captured
};// END class block

//START class boundary
class Boundary{
public:
    std::set<Point>                   _boundaryPoints;

    // Actions:
    void createBoundary(); //when stone placed on board, check for free spots, adds the free ones to boundary
    void addBoundary();//adds one stones boundaries to another stone
    void removeBoundaryPoint(); //removes the boundary_point to stone_1 if stone_2 is on it
    void removeBoundary();// removes the boundary when captured
};// END class boundary

//START class Board
class Board : private priv::Board_base {
public:
    // Enable types
    using                             Board_base::BoardData;
    using                             Board_base::Position;
    // Enable constructors
    using                             Board_base::Board_base;
    // Make visible from Board_base
    using                             Board_base::resetBoard;
    using                             Board_base::size;
    using                             Board_base::wasPreviousPass;
    using                             Board_base::turn;
    // Validate
    bool                              isNextPositionValid(Point intersection ) const;
    bool                              isSuicide(Point intersection) const; // check suicide
    bool                              isFree(Point point) const; //check if free
    bool                              currentStone(Point intersection) const;
    // Actions    _board->turn(), intersection, _board
    void                              placeStone(std::shared_ptr<Stone>  stone);
    void                              passTurn();
    // Stones and positions
    bool                              hasStone(Point intersection ) const;
    std::shared_ptr<Stone>                        stone(Point intersection ) const;
}; // END class Board

// START class Engine
class Engine : public std::enable_shared_from_this<Engine> {
public:
    Engine();
    // Actions
    void                              newGame( Size size );
    void                              newGameVsAi( Size size );
    void                              newGameAiVsAi( Size size );
    void                              newGameFromState( Board::BoardData&& board,
                                                        StoneColor turn,
                                                        bool was_previous_pass );
    void                              passTurn();
    void                              nextTurn( time_type think_time = 100_ms );

    const std::shared_ptr<Board>      board() const;
    StoneColor                        turn() const;
    const GameMode&                   gameMode() const;
    const std::shared_ptr<Player>     currentPlayer() const;
    // Validate
    bool                              placeStone( Point intersection );
    bool                              validateStone( Point intersection ) const;
    bool                              isGameActive() const;
private:
    std::shared_ptr<Board>            _board;
    GameMode                          _game_mode;
    bool                              _active_game;
    std::shared_ptr<Player>           _white_player;
    std::shared_ptr<Player>           _black_player;
}; // END class Engine
} // END namespace go
} // END namespace mylib

#endif //GO_H
