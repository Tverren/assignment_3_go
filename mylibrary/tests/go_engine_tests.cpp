
// My Go Engine
#include "../../mylibrary/go.h"

// gtest
#include <gtest/gtest.h>


namespace go = mylib::go;


TEST(GoEngineTest_Board,FromStateConstructor) {

    //       1  2  3  4  5
    //      |x  x  x  x  x| 1
    //      |x  x  x  x  x| 2
    //      |x  x  b  x  x| 3
    //      |x  x  x  x  x| 4
    //      |x  x  x  x  x| 5

    go::Board::BoardData board_data;
    auto stone = std::make_shared<go::Stone> (go::StoneColor::Black, go::Point(3,3), nullptr);
    board_data[go::Point(3,3)] = stone;

    // Create a engine from state
    go::Board board {std::move(board_data),go::StoneColor::White,false};

    // EXPECT a stone at (3,3)
    EXPECT_TRUE(board.hasStone({3,3}));

    // EXPECT a black stone at (3,3)
    EXPECT_TRUE(board.stone({3,3}) == stone);
}

TEST(GoEngineTest_LegalStonePlacements,SimpleKoRuleTest) {

    //       1  2  3  4  5
    //      |x  x  x  x  x| 1
    //      |x  x  w  x  x| 2
    //      |x  w  b  w  x| 3
    //      |x  b  x  b  x| 4
    //      |x  x  b  x  x| 5


    go::StoneColor B = go::StoneColor::Black;
    go::StoneColor W = go::StoneColor::White;

    go::Board::BoardData b;
    b[{3,2}] = std::make_shared<go::Stone>(W,go::Point(3,2), nullptr);
    b[{2,3}] = std::make_shared<go::Stone>(W,go::Point(2,3), nullptr);
    b[{3,3}] = std::make_shared<go::Stone>(B,go::Point(3,3), nullptr);
    b[{4,3}] = std::make_shared<go::Stone>(W,go::Point(4,3), nullptr);
    b[{2,4}] = std::make_shared<go::Stone>(B,go::Point(2,4), nullptr);
    b[{4,4}] = std::make_shared<go::Stone>(B,go::Point(4,4), nullptr);
    b[{3,5}] = std::make_shared<go::Stone>(B,go::Point(3,5), nullptr);


    // Create a PvP engine
    auto engine = std::make_shared<go::Engine>();
    engine->newGameFromState( std::move(b), go::StoneColor::White, false );

    // Set up the game for the 'ko' rule to trigger:
    // white removes the freedoms of black's Stone on (3,3)
    // and captures it.
    engine->placeStone(go::Point(3,4));

    // Play a illegal move for black triggering the 'ko' rule
    // when black tries to capture whites (3,4).
    EXPECT_FALSE(engine->validateStone(go::Point(3,4)));
}

//checks the color of the stone
TEST(GoStoneColor, SimpleColorTest) {

    //       1  2  3  4  5
    //      |x  x  x  x  x| 1
    //      |x  x  x  x  x| 2
    //      |x  x  b  x  x| 3
    //      |x  x  x  x  x| 4
    //      |x  x  x  x  x| 5

    go::StoneColor B = go::StoneColor::Black;
    go::StoneColor W = go::StoneColor::White;

    // Create empty 5x5 board
    auto board = std::make_shared<go::Board>(go::Size(5,5));

    //creates stones at given x and y position
    auto stoneWhite = std::make_shared<go::Stone>(W, go::Point(3,2), board);
    auto stoneBlack = std::make_shared<go::Stone>(B, go::Point(3,3), board);


    //places stones at given x and y position
    board->placeStone(stoneWhite);
    board->placeStone(stoneBlack);

    // EXPECT a stone at (3,2)
    EXPECT_TRUE(board->hasStone({3,2}));
    // EXPECT a stone at (3,3)
    EXPECT_TRUE(board->hasStone({3,3}));

    // EXPECT a white stone at (3,3)
    EXPECT_TRUE(board->stone({3,2}) == stoneWhite);
    // EXPECT a black stone at (3,3)
    EXPECT_TRUE(board->stone({3,3}) == stoneBlack);

    // EXPECT NOT a black stone at (3,2)
    EXPECT_FALSE(board->stone({3,2}) == stoneBlack);
    // EXPECT not a white stone at (3,3)
    EXPECT_FALSE(board->stone({3,3}) == stoneWhite);
}

//checks the color of the stones
TEST(GoStoneColor, SimpleAllegianceTest) {

    //       1  2  3  4  5
    //      |x  x  B  x  x| 1
    //      |x  B  W  B  x| 2
    //      |x  x  B  x  x| 3
    //      |x  x  x  x  x| 4
    //      |x  x  x  x  x| 5

    go::StoneColor B = go::StoneColor::Black;
    go::StoneColor W = go::StoneColor::White;

    // Create empty 5x5 board
    auto board = std::make_shared<go::Board>(go::Size(5,5));


    auto currentStone = std::make_shared<go::Stone>(W, go::Point(3,2), board);//placing stone

    //creates stones at given x and y position
    auto stoneWhite = std::make_shared<go::Stone>(W, go::Point(3,2), board);
    auto stoneBlack = std::make_shared<go::Stone>(B, go::Point(3,3), board);
    auto stoneBlack1 = std::make_shared<go::Stone>(B, go::Point(2,2), board);
    auto stoneBlack2 = std::make_shared<go::Stone>(B, go::Point(4,2), board);
    auto stoneBlack3 = std::make_shared<go::Stone>(B, go::Point(3,1), board);

    //places stones at given x and y position

    board->placeStone(stoneBlack);
    board->placeStone(stoneBlack1);
    board->placeStone(stoneBlack2);
    board->placeStone(stoneBlack3);
    board->placeStone(stoneWhite);

    // EXPECT NOT A FREEDOM
    EXPECT_FALSE(board->currentStone({3,2}));

    //EXPECT THAT POINT ISKO
    EXPECT_TRUE((board->isSuicide(currentStone->Spot())));

    //EXPECT_TRUE(board->isKo(currentStone->Spot()));

    //EXPECT_TRUE(board->isKo(currentStone->Spot()));

    // EXPECT a white stone at (3,2)
    EXPECT_TRUE(board->stone({3,2}) == stoneWhite);
}

//checks the color of the stones
TEST(GoStoneColor, SimpleAllegianceTestInUpperRightCorner) {

    //       1  2  3  4  5
    //      |x  x  x  B  W| 1
    //      |x  x  x  x  B| 2
    //      |x  x  x  x  x| 3
    //      |x  x  x  x  x| 4
    //      |x  x  x  x  x| 5

    go::StoneColor B = go::StoneColor::Black;
    go::StoneColor W = go::StoneColor::White;

    // Create empty 5x5 board
    auto board = std::make_shared<go::Board>(go::Size(5,5));

    //creates stones at given x and y position
    auto stoneWhite = std::make_shared<go::Stone>(W, go::Point(5,1), board);
    auto stoneBlack = std::make_shared<go::Stone>(B, go::Point(4,1), board);
    auto stoneBlack1 = std::make_shared<go::Stone>(B, go::Point(5,2), board);


    //places stones at given x and y position
    board->placeStone(stoneBlack);
    board->placeStone(stoneBlack1);
    board->placeStone(stoneWhite);

    // EXPECT NO FREEDOMS
    EXPECT_FALSE(board->currentStone({5,1}));
    // EXPECT a white stone at (5,1)
    EXPECT_TRUE(board->stone({5,1}) == stoneWhite);
}

//checks the color of the stones
TEST(GoStoneColor, SimpleAllegianceTestInUpperLeftCorner) {

    //       1  2  3  4  5
    //      |W  B  x  x  x| 1
    //      |B  x  x  x  x| 2
    //      |x  x  x  x  x| 3
    //      |x  x  x  x  x| 4
    //      |x  x  x  x  x| 5

    go::StoneColor B = go::StoneColor::Black;
    go::StoneColor W = go::StoneColor::White;

    // Create empty 5x5 board
    auto board = std::make_shared<go::Board>(go::Size(5,5));

    //creates stones at given x and y position
    auto stoneWhite = std::make_shared<go::Stone>(W, go::Point(1,1), board);
    auto stoneBlack = std::make_shared<go::Stone>(B, go::Point(2,1), board);
    auto stoneBlack1 = std::make_shared<go::Stone>(B, go::Point(2,1), board);


    //places stones at given x and y position
    board->placeStone(stoneBlack);
    board->placeStone(stoneBlack1);
    board->placeStone(stoneWhite);

    // EXPECT NO FREEDOMS
    EXPECT_FALSE(board->currentStone({1,1}));
    // EXPECT a white stone at (1,1)
    EXPECT_TRUE(board->stone({1,1}) == stoneWhite);
}

//checks the color of the stones
TEST(GoStoneColor, SimpleAllegianceTestInLowerRightCorner) {

    //       1  2  3  4  5
    //      |x  x  x  x  x| 1
    //      |x  x  x  x  x| 2
    //      |x  x  x  x  x| 3
    //      |x  x  x  x  B| 4
    //      |x  x  x  B  W| 5

    go::StoneColor B = go::StoneColor::Black;
    go::StoneColor W = go::StoneColor::White;

    // Create empty 5x5 board
    auto board = std::make_shared<go::Board>(go::Size(5,5));

    //creates stones at given x and y position
    auto stoneWhite = std::make_shared<go::Stone>(W, go::Point(5,5), board);
    auto stoneBlack = std::make_shared<go::Stone>(B, go::Point(5,4), board);
    auto stoneBlack1 = std::make_shared<go::Stone>(B, go::Point(4,5), board);


    //places stones at given x and y position
    board->placeStone(stoneBlack);
    board->placeStone(stoneBlack1);
    board->placeStone(stoneWhite);

    // EXPECT NO FREEDOMS
    EXPECT_FALSE(board->currentStone({5,5}));
    // EXPECT a white stone at (5,5)
    EXPECT_TRUE(board->stone({5,5}) == stoneWhite);
}

//checks the color of the stones
TEST(GoStoneColor, SimpleAllegianceTestInLowerLeftCorner) {

    //       1  2  3  4  5
    //      |x  x  x  x  x| 1
    //      |x  x  x  x  x| 2
    //      |x  x  x  x  x| 3
    //      |B  x  x  x  x| 4
    //      |W  B  x  x  x| 5

    go::StoneColor B = go::StoneColor::Black;
    go::StoneColor W = go::StoneColor::White;

    // Create empty 5x5 board
    auto board = std::make_shared<go::Board>(go::Size(5,5));

    //creates stones at given x and y position
    auto stoneWhite = std::make_shared<go::Stone>(W, go::Point(1,5), board);
    auto stoneBlack = std::make_shared<go::Stone>(B, go::Point(1,4), board);
    auto stoneBlack1 = std::make_shared<go::Stone>(B, go::Point(2,5), board);


    //places stones at given x and y position
    board->placeStone(stoneBlack);
    board->placeStone(stoneBlack1);
    board->placeStone(stoneWhite);

    // EXPECT NO FREEDOMS
    EXPECT_FALSE(board->currentStone({1,5}));
    // EXPECT a white stone at (1,5)
    EXPECT_TRUE(board->stone({1,5}) == stoneWhite);
}

//checks the intersection between black and white
TEST(GoPostion, intersection)
{

    //       1  2  3  4  5
    //      |x  x  x  x   x| 1
    //      |x  x  x  x   x| 2
    //      |x  x BW  x   x| 3
    //      |x  x  x  x   x| 4
    //      |x  x  x  x   x| 5

    //Black stone at point (3,3)
    go::Board::BoardData board_data;
    board_data[go::Point(3,3)] = std::make_shared<go::Stone>(go::StoneColor::Black, go::Point(3,3), nullptr);

    // Create a engine from state
    go::Board board {std::move(board_data),go::StoneColor::White,false};

    //EXPECT FALSE, a white stone stone on point (3,3) (intersection)
    EXPECT_FALSE(board.isNextPositionValid(go::Point(3,3)));
}

//checks if south has north
TEST(GoPosition, hasNorth) {

    //       1  2  3  4  5
    //      |x  x  x  x  x| 1
    //      |x  x  x  x  x| 2
    //      |x  x  x  x  x| 3
    //      |x  x  x  x  Nw| 4
    //      |x  x  x  x  Sw| 5

    go::StoneColor W = go::StoneColor::White;

    // Create empty 5x5 board
    auto board = std::make_shared<go::Board>(go::Size(5,5));

    //creates stones at given x and y position
    auto stoneNorth = std::make_shared<go::Stone>(W, go::Point(5,4), board);
    auto stoneSouth = std::make_shared<go::Stone>(W, go::Point(5,5), board);

    //places stones at given x and y position
    board->placeStone(stoneNorth);
    board->placeStone(stoneSouth);

    //EXPECT TO HAVE STONE AT NORTH
    EXPECT_TRUE(stoneSouth->hasNorth());

    //EXPECT TO NOT HAVE STONES AT SOUTH, EAST AND WEST
    EXPECT_FALSE(stoneSouth->hasSouth());

    EXPECT_FALSE(stoneSouth->hasEast());

    EXPECT_FALSE(stoneSouth->hasWest());
}

//checks if west has east
TEST(GoPosition, hasEast) {

    //       1  2  3  4  5
    //      |x  x  x  x  x| 1
    //      |x  x  x  x  x| 2
    //      |x  Ww Ew x  x| 3
    //      |x  x  x  x  x| 4
    //      |x  x  x  x  x| 5

    go::StoneColor W = go::StoneColor::White;

    // Create empty 5x5 board
    auto board = std::make_shared<go::Board>(go::Size(5,5));

    //creates stones at given x and y position
    auto stoneEast = std::make_shared<go::Stone>(W, go::Point(3,3), board);
    auto stoneWest = std::make_shared<go::Stone>(W, go::Point(2,3), board);

    //places stones at given x and y position
    board->placeStone(stoneEast);
    board->placeStone(stoneWest);

    //EXPECT TO HAVE STONE AT EAST
    EXPECT_TRUE(stoneWest->hasEast());

    //EXPECT TO NOT HAVE STONES AT SOUTH, NORTH AND WEST
    EXPECT_FALSE(stoneWest->hasSouth());

    EXPECT_FALSE(stoneWest->hasNorth());

    EXPECT_FALSE(stoneWest->hasWest());
}

//check if east has west
TEST(GoPosition, hasWest) {

    //       1  2  3  4  5
    //      |x  x  x  x  x| 1
    //      |x  x  x  x  x| 2
    //      |x  Ww Ew x  x| 3
    //      |x  x  x  x  x| 4
    //      |x  x  x  x  x| 5

    go::StoneColor W = go::StoneColor::White;

    // Create empty 5x5 board
    auto board = std::make_shared<go::Board>(go::Size(5,5));

    //creates stones at given x and y position
    auto stoneWest = std::make_shared<go::Stone>(W, go::Point(2,3), board);
    auto stoneEast = std::make_shared<go::Stone>(W, go::Point(3,3), board);

    //places stones at given x and y position
    board->placeStone(stoneWest);
    board->placeStone(stoneEast);

    //EXPECT TO HAVE STONE AT WEST
    EXPECT_TRUE(stoneEast->hasWest());

    //EXPECT TO NOT HAVE STONES AT SOUTH, EAST AND NORTH
    EXPECT_FALSE(stoneEast->hasSouth());

    EXPECT_FALSE(stoneEast->hasEast());

    EXPECT_FALSE(stoneEast->hasNorth());
}

//checks if north has south
TEST(GoPosition, hasSouth) {

    //       1  2  3  4  5
    //      |x  x  x  x  x| 1
    //      |x  x  x  x  x| 2
    //      |x  x  x  x Nw| 3
    //      |x  x  x  x Sw| 4
    //      |x  x  x  x  x| 5

    go::StoneColor W = go::StoneColor::White;

    // Create empty 5x5 board
    auto board = std::make_shared<go::Board>(go::Size(5,5));

    //creates stones at given x and y position
    auto stoneNorth = std::make_shared<go::Stone>(W, go::Point(5,3), board);
    auto stoneSouth = std::make_shared<go::Stone>(W, go::Point(5,4), board);

    //places stones at given x and y position
    board->placeStone(stoneNorth);
    board->placeStone(stoneSouth);

    //EXPECT TO HAVE STONE AT SOUTH
    EXPECT_TRUE(stoneNorth->hasSouth());

    //EXPECT TO NOT HAVE STONES AT NORTH, EAST AND WEST
    EXPECT_FALSE(stoneNorth->hasNorth());

    EXPECT_FALSE(stoneNorth->hasEast());

    EXPECT_FALSE(stoneNorth->hasWest());
}

//checks if stone intersection is surrounded
TEST(GoPosition, surroundingStones) {

    //       1  2  3  4  5
    //      |x  x  x  x  x| 1
    //      |x  x  N  x  x| 2
    //      |x  W  C  E  x| 3
    //      |x  x  S  x  x| 4
    //      |x  x  x  x  x| 5

    go::StoneColor W = go::StoneColor::White;

    // Create empty 5x5 board
    auto board = std::make_shared<go::Board>(go::Size(5,5));

    //creates stones at given x and y position
    auto currentStone = std::make_shared<go::Stone>(W, go::Point(3,3), board);//placing stone

    auto stoneNorth = std::make_shared<go::Stone>(W, go::Point(3,2), board);
    auto stoneSouth = std::make_shared<go::Stone>(W, go::Point(3,4), board);
    auto stoneEast = std::make_shared<go::Stone>(W, go::Point(4,3), board);
    auto stoneWest = std::make_shared<go::Stone>(W, go::Point(2,3), board);

    //places stones at given x and y position
    board->placeStone(stoneNorth);
    board->placeStone(stoneSouth);
    board->placeStone(stoneEast);
    board->placeStone(stoneWest);

    //EXPECT
    //EXPECT_TRUE(stoneSouth->hasNorth());

    //EXPECT SPOT TO BE SURRONDED by stones
    EXPECT_TRUE(board->isSuicide(currentStone->Spot()));
}

//tries to place stone at given intersection
TEST(GoPosition, triesMiddleSuicide) {

    //       1  2  3  4  5
    //      |x  x  x  x  x| 1
    //      |x  x  N  x  x| 2
    //      |x  W  C  E  x| 3
    //      |x  x  S  x  x| 4
    //      |x  x  x  x  x| 5

    go::StoneColor W = go::StoneColor::White;

    go::Board::BoardData b;
    b[{3,2}] = std::make_shared<go::Stone>(W,go::Point(3,2), nullptr);
    b[{2,3}] = std::make_shared<go::Stone>(W,go::Point(2,3), nullptr);
    b[{3,4}] = std::make_shared<go::Stone>(W,go::Point(3,4), nullptr);
    b[{4,3}] = std::make_shared<go::Stone>(W,go::Point(4,3), nullptr);


    // Create a PvP engine
    auto engine = std::make_shared<go::Engine>();
    engine->newGameFromState( std::move(b), go::StoneColor::White, false );


    //tries to place a stone at given x and y position
    auto currentStone = std::make_shared<go::Stone>(W, go::Point(3,3), engine->board());

    engine->placeStone(currentStone->Spot());

    //EXPECT SPOT TO BE SURRONDED BY STONES
    EXPECT_TRUE(engine->board()->isSuicide(currentStone->Spot()));

    //EXPECT NOT TO FIND A STONE IN GIVEN SPOT
    EXPECT_FALSE(engine->board()->hasStone(currentStone->Spot()));
}

//tries to place stone at given intersection
TEST(GoPosition, triesCornerSuicide) {

    //       1  2  3  4  5
    //      |x  x  x  x  x| 1
    //      |x  x  x  x  x| 2
    //      |x  x  x  x  x| 3
    //      |x  x  x  x  N| 4
    //      |x  x  x  W  C| 5

    go::StoneColor W = go::StoneColor::White;

    go::Board::BoardData b;
    b[{5,4}] = std::make_shared<go::Stone>(W,go::Point(5,4), nullptr);
    b[{4,5}] = std::make_shared<go::Stone>(W,go::Point(4,5), nullptr);

    // Create empty 5x5 board
    //auto board = std::make_shared<go::Engine>(go::Size(5,5));

    // Create a PvP engine
    auto engine = std::make_shared<go::Engine>();
    engine->newGameFromState( std::move(b), go::StoneColor::White, false );


    //creates stones at given x and y position
    auto currentStone = std::make_shared<go::Stone>(W, go::Point(5,5), engine->board());//placing stone

    engine->placeStone(currentStone->Spot());

    //EXPECT SPOT TO BE SURRONDED BY STONES
    EXPECT_TRUE(engine->board()->isSuicide(currentStone->Spot()));

    //EXPECT NOT TO FIND A STONE IN GIVEN SPOT
    EXPECT_FALSE(engine->board()->hasStone(currentStone->Spot()));
}

